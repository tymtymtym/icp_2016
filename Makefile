COMPILER=g++
DEPDIR=dep
SRCDIR=src
OBJDIR=obj
INCDIR=include
OBJLIB=$(OBJDIR)/objlib.a
QTINCDIR=$(shell qmake -query QT_INSTALL_HEADERS)
QTLIBDIR=$(shell qmake -query QT_INSTALL_LIBS)
QTBINDIR=$(shell qmake -query QT_INSTALL_BINS)
QTLDFLAGS=-L$(QTLIBDIR) -lQt5Core -lQt5Gui -lQt5Widgets
MOC=$(QTBINDIR)/moc
MOCFILEDIR=moc
#makes sure that DEPDIR and OBJDIR exist
$(shell mkdir -p $(DEPDIR))
$(shell mkdir -p $(OBJDIR))
$(shell mkdir -p $(MOCFILEDIR))
DEPFLAGS = -MT $@ -MMD -MP -MF $(DEPDIR)/$*.Td
OFILES =	$(patsubst $(SRCDIR)/%.cpp,$(OBJDIR)/%.cpp.o,$(wildcard $(SRCDIR)/*.cpp))
MOCOFILES = $(patsubst $(INCDIR)/%.qt.h,$(OBJDIR)/%.mcpp.o,$(wildcard $(INCDIR)/*.qt.h))
LDFLAGS= -pthread
INCFLAGS=-I $(INCDIR) -isystem $(QTINCDIR) -isystem $(QTINCDIR)/QtWidgets \
				 -isystem $(QTINCDIR)/QtGui

CFLAGS=-x c++ -std=c++11 -Wall -Wextra -pedantic -fPIC

all: CFLAGS+=-O3 -DNDEBUG
all: hra2016-cli hra2016

debug: CFLAGS+=-g
debug: hra2016-cli hra2016

.PRECIOUS: $(MOCFILEDIR)/%.mcpp

.PHONY: clean pack doc doxygen run

POSTCOMPILE = mv -f $(DEPDIR)/$*.Td $(DEPDIR)/$*.d

hra2016-cli: $(OBJDIR)/main_cli.cpp.o $(OBJLIB)
	$(COMPILER) $^ -o $@ $(LDFLAGS)

hra2016: LDFLAGS+=$(QTLDFLAGS)
hra2016: $(OBJDIR)/main_gui.cpp.o $(OBJLIB)
	$(COMPILER) $^ -o $@ $(LDFLAGS)

$(OBJLIB): $(OFILES) $(MOCOFILES)
	ar rvs $@ $^

#the .d dependency is here so that dependencies are rebuilt
#in case they are deleted
$(OBJDIR)/%.cpp.o : $(SRCDIR)/%.cpp $(DEPDIR)/%.d
	$(COMPILER) $(CFLAGS) $(INCFLAGS) $(DEPFLAGS) -c -o $@ $<
	$(POSTCOMPILE)

$(MOCFILEDIR)/%.mcpp : $(INCDIR)/%.qt.h
	$(MOC) -I $(INCDIR) -I $(QTINCDIR) -I $(QTINCDIR)/QtWidgets $< -o $@

$(OBJDIR)/%.mcpp.o : $(MOCFILEDIR)/%.mcpp
	$(COMPILER) $(CFLAGS) $(INCFLAGS) -c -o $@ $<

#this is here so that make doesn't freak out when a .d file
#does not exist 
$(DEPDIR)/%.d: ;

clean: 
	@rm -rf $(OBJDIR) $(DEPDIR) $(MOCFILEDIR) hra2016 hra2016-cli doc/*

run:
	./hra2016
	./hra2016-cli

doxygen: doc

doc:
	doxygen

pack:
	rm -rf doc/*
	zip -r xvitra00-xzenca00.zip src/*.cpp img include/*.h examples doc README.txt Makefile Doxyfile

#this includes the .d files generated in compilation
#they contain rules in format name.o: srcfile headers
#also header:
#but that's not important
-include $(patsubst $(OBJDIR)/%.cpp.o,$(DEPDIR)/%.d,$(OFILES))
