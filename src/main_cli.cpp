/*!
 * \file main_cli.cpp
 *
 * \brief Conatins main for hra2016-cli.
 */
#include <UI_cli.h>
/*!
 * \brief Runs UiCli::menu() and returns 0 when it's done.
 */
int main()
{
	icp::UiCli ui;
	ui.run();
	return 0;
}
/*** End of file main_cli.cpp ***/
