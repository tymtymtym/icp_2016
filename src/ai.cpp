/*!
 * \file ai.cpp
 *
 * \brief Contains definitions of Ai related methods of classes defined in ai.h.
 * \author Radek Vit
 */
#include<limits>
#include<algorithm>
#include"../include/ai.h"
#include<random>

/*!
 * \author Radek Vit
 * Rates state of board from Ai's perspective. All tiles are counted.
 * Tiles in good positions add to the rating, tiles in bad positions subtract
 * from it.
 */
int icp::Board_ai::heuristic()
{
	const int T_OFFSET = size*2-2;
	//total score
	int white_t = count_white();
	int black_t = count_black();
	//good/bad score
	int white_g = 0;
	int black_g = 0;

	int heuristic_t = 0;
	int heuristic_g = 0;

	int t_size = size*size - 4 + T_OFFSET;
	int distance = t_size + 4 - white_t - black_t;
	Tile tile = {Tile::EMPTY};

	if(playerTurn == Turn::TIE)
		return TIE_HEURISTIC;
	else if(playerTurn == Turn::BLACKWIN)
	{
		if(amIWhite)
			return std::numeric_limits<int>::min();
		else
			return std::numeric_limits<int>::max();
	}
	else if(playerTurn == Turn::WHITEWIN)
	{
		if(amIWhite)
			return std::numeric_limits<int>::max();
		else
			return std::numeric_limits<int>::min();
	}
	//adding for best positions	
	for(Coordinates coord: best)
	{
		tile = board[map(coord.x,coord.y)];
		if(tile == Tile::WHITE)
			white_g += 4;
		else if(tile == Tile::BLACK)
			black_g += 4;
	}
	//adding for good positions
	for(Coordinates coord: good)
	{
		tile = board[map(coord.x,coord.y)];
		if(tile == Tile::WHITE)
			white_g += 1;
		else if(tile == Tile::BLACK)
			black_g += 1;
	}
	//subtractiong for bad positions
	for(Coordinates coord: bad)
	{
		tile = board[map(coord.x,coord.y)];
		if(tile == Tile::WHITE)
			white_g -= 1;
		else if(tile == Tile::BLACK)
			black_g -= 1;
	}

	heuristic_t =  amIWhite ? (white_t - black_t) : (black_t - white_t);
	heuristic_g =  amIWhite ? (white_g - black_g) : (black_g - white_g);

	return heuristic_g * WEIGHT * distance + heuristic_t * (t_size - distance);
}
/*!
 * \author Radek Vit
 * Returns best found move. Uses alphabeta algorhitm, first marks best move
 * during evaluation.
 */
icp::Coordinates icp::Ai_alphabeta::first(const Board &g,int max_depth)
{
	int a = std::numeric_limits<int>::min();
	int b = std::numeric_limits<int>::max();
	int x;
	max_depth *= INCREMENT_BIG; //set max_depth to max_depth big increments
	icp::Board_ai board{g};
	Coordinates result{board.moves()[0]};
	for(Coordinates c:board.moves())
	{
		board.play(c.x,c.y); //make move
		switch(board.player_ai())
		{
			case PLAYER1:
				x = alpha(board,a,b,INCREMENT_SMALL,max_depth);
			break;
			case PLAYER2:
				x = beta(board,a,b,INCREMENT_BIG,max_depth);
			break;
			default:
				x = board.heuristic();
			break;
		}
		if(x > a)
		{
			a = x;
			result = c;
		}
		if(a == b)
			return result;
		board.undo();
	}

	return result;
}

/*!
 * \author Radek Vit
 * Alpha from alphabeta. Expands to all possible states and calls correct methods
 * to evaluate them. Chooses best evaluation to return.
 */
int icp::Ai_alphabeta::alpha(Board_ai &board,int a,int b,int depth,int max_depth)
{
	int x;

	if(depth >= max_depth)
		return board.heuristic();
	
	for(Coordinates c:board.moves()) //next possible move
	{
		board.play(c.x,c.y); //make move
		switch(board.player_ai())
		{
			case PLAYER1: //alpha
			/* repeated alpha has better potential */
				x = alpha(board,a,b,depth+INCREMENT_SMALL,max_depth);
			break;
			case PLAYER2:
				x = beta(board,a,b,depth+INCREMENT_BIG,max_depth);
			break;
			default:
				return board.heuristic();
			break;
		}
		board.undo();
		if(x > a)
			a = x;
		if(a >= b)
			return a;
	}
	return a;
}

/*!
 * \author Radek Vit
 * Beta from alphabeta. Expands to all possible states and calls correct methods
 * to evaluate them. Chooses worst evaluation to return.
 */
int icp::Ai_alphabeta::beta(Board_ai &board,int a,int b,int depth, int max_depth)
{
	int x;

	if(depth >= max_depth)
		return board.heuristic();
	
	for(Coordinates c:board.moves()) //next possible move
	{
		board.play(c.x,c.y); //make move
		switch(board.player_ai())
		{
			case PLAYER1: //alpha
				x = alpha(board,a,b,depth+INCREMENT_BIG,max_depth);
			break;
			case PLAYER2:
				x = beta(board,a,b,depth+INCREMENT_SMALL,max_depth);
			break;
			default:
				return board.heuristic();
			break;
		}
		board.undo();
		if(x < b)
			b = x;
		if(a >= b)
			return b;
	}
	return b;	
}

/*!
 * \author Radek Vit
 * Offers a move using standard C++ uniform distribution and rng.
 */
icp::Coordinates icp::Ai_random::offer(const Board &b)
{
	if(b.player() != Turn::WHITE && b.player() != Turn::BLACK)
		throw std::runtime_error(
			"Ai cannot offer a move for a finished game");

	std::vector<Coordinates> vec(b.moves());
	std::uniform_int_distribution<> dist(0,vec.size() - 1);

	return vec[dist(rng)];
}

/*** End of file ai.cpp ***/
