/*!
 * \file board.cpp
 *
 * \brief Implements non-inline methods of class Board
 * \author Radek Vit
 */

#include"board.h"
#include<stdexcept>
#include<string>
using Vector = icp::Coordinates;
const int VECTORS_COUNT{8};

/*!
 * \brief A constant array of direction vectors.
 */
const Vector direction[VECTORS_COUNT] = {
	{1,0},{1,1},{0,1},{-1,1},{-1,0},{-1,-1},{0,-1},{1,-1}
};

/*!
 * Turns last played tile EMPTY and turns each tile between tile played and
 * endpoints from history to other player's corresponding color.
 * Does nothing if there is no turn to undo.
 * \returns Player who is on turn after undo.
 * \author Radek Vit
 */
icp::Turn icp::Board::undo()
{
	if(positionInHistory == 0)
		return playerTurn;

	const historyEntry &move = history[positionInHistory-1];
	positionInHistory--;

	const Tile me = board[map(move.playedX,move.playedY)];
	const Tile him = (me == Tile::BLACK) ? Tile::WHITE : Tile::BLACK;

	board[map(move.playedX,move.playedY)] = Tile::EMPTY;

	for(int i = 0; i < MAX_ENDPOINTS && move.endPointsX[i] != -1;i++)
	{
		int vecx = unitise(move.endPointsX[i] - move.playedX);
		int vecy = unitise(move.endPointsY[i] - move.playedY);
		int x = move.playedX + vecx;
		int y = move.playedY + vecy;

		while(x != move.endPointsX[i] || y != move.endPointsY[i])
		{
			board[map(x,y)] = him;
			x += vecx;
			y += vecy;
		}
	}

	playerTurn = (me == Tile::BLACK) ? Turn::BLACK : Turn::WHITE;
	return playerTurn;
}

/*!
 * Simulates play() using next history entry, but does not erase forward history.
 * Figures out who is on turn from second forward history entry if available.
 * If only one forward history entry is available, uses Board::play() and
 * returns.
 * If there is no forward history entry available does nothing and returns.
 * \returns Player on turn after redo or who won.
 * \author Radek Vit
 */
icp::Turn icp::Board::redo()
{
	if(positionInHistory == history.size())
		return playerTurn;

	const historyEntry &move = history[positionInHistory];
/* if redoing to last point in history play is used to make the play */
	if(positionInHistory + 1 == history.size())
		return play(move.playedX,move.playedY);
	
	positionInHistory++;

	const Tile me = (playerTurn == Turn::BLACK) ? Tile::BLACK : Tile::WHITE;

	board[map(move.playedX,move.playedY)] = me;

	for(int i=0;i<MAX_ENDPOINTS && move.endPointsX[i] != -1;i++)
	{
		int vecx = unitise(move.endPointsX[i] - move.playedX);
		int vecy = unitise(move.endPointsY[i] - move.playedY);
		int x = move.playedX + vecx;
		int y = move.playedY + vecy;

		while(x != move.endPointsX[i] || y != move.endPointsY[i])
		{
			board[map(x,y)] = me;
			x += vecx;
			y += vecy;
		}
	}

/* getting turn from history */
	int x = history[positionInHistory].endPointsX[0];
	int y = history[positionInHistory].endPointsY[0];
	playerTurn = (board[map(x,y)] == Tile::BLACK) ? Turn::BLACK : Turn::WHITE;

	return playerTurn;
}

/*!
 * If ox or oy are out of bounds, play() does nothing.
 * Deletes all forward history, even when a turn is ultimately unsuccesful.
 * Creates new history entry for the turn played.
 * Each endpoint is added to this turn's history entry.
 * Sets player by testing move availability. If no moves are available, sets
 * player to whoever won.
 * \param[in] ox X-coordinate of tile played.
 * \param[in] oy Y-coordinate of tile played.
 * \returns Player on turn after play or who won.
 * \author Radek Vit
 */
icp::Turn icp::Board::play(const int ox, const int oy)
{
	const Turn meTurn = playerTurn;
	const Turn himTurn = (playerTurn == Turn::WHITE) ? Turn::BLACK : Turn::WHITE;
	const Tile me = (playerTurn == Turn::BLACK) ? Tile::BLACK : Tile::WHITE;
	const Tile him = (playerTurn == Turn::BLACK) ? Tile::WHITE : Tile::BLACK;
	int x = ox, y = oy;
	
	Tile current;
	bool modified;
	int history_index = 0;
	
	if(playerTurn != Turn::BLACK && playerTurn != Turn::WHITE)
		return playerTurn;

	if(board[map(x,y)] != Tile::EMPTY || !is_in_bounds(x,y))
		return playerTurn;

/* throwing away forward history */
	while(positionInHistory != history.size())
		history.pop_back();
/* adding entry to history */
	history.push_back(
		{x,y,{-1,-1,-1,-1,-1,-1,-1,-1},{-1,-1,-1,-1,-1,-1,-1,-1}}
		);
/* playing in each direction if possible */
	for(int i = 0; i < VECTORS_COUNT; i++ )
	{
		x = ox;
		y = oy;
		int steps = 0;
		do
		{
			steps++;
			x += direction[i].x;
			y += direction[i].y;
			current = get_board_value(x,y);
		} while (current == him);
/* play was succesful in this direction, turning tiles to player's color */
		if(current == me && steps > 1)
		{
			modified = true;
			// marking endpoint to history entry
			history.back().endPointsX[history_index] = x;
			history.back().endPointsY[history_index++] = y;

			x = ox + direction[i].x;
			y = oy + direction[i].y;
			current = get_board_value(x,y);
			while (current == him)
			{
				board[map(x,y)] = me;

				x += direction[i].x;
				y += direction[i].y;
				current = get_board_value(x,y);
			}
		}
	}
	if(!modified) // turn unsuccessful
	{
		history.pop_back(); // removing history entry of unsuccessful turn
		return playerTurn;
	}
	
	positionInHistory++;
	board[map(ox,oy)] = me;
/* figuring out who is on turn */
	playerTurn = himTurn;

	if(move_available()) // at least one turn is available for other player
	{
		return himTurn;
	}
	else // player who just played plays again or someone won
	{
		playerTurn = meTurn;
		if(move_available()) // player who just played plays again
		{
			return meTurn;
		}
		else // game is finished
		{
			int white = count_white();
			int black = count_black();
			if(black > white)
				playerTurn = Turn::BLACKWIN;
			else if(black < white)
				playerTurn = Turn::WHITEWIN;
			else if(black == white)
				playerTurn = Turn::TIE;
			
			return playerTurn; 
		}
	}

}

/*!
 * Simulates playing in each direction without affecting the board.
 * Returns true when a playable direction is found.
 * Returns false when x,y is out of range or isn't EMPTY
 * \returns Playability of tile x,y.
 * \param[in] ox X-coordinate of examined tile
 * \param[in] oy Y-coordinate of examined tile
 * \author Radek Vit
 */
bool icp::Board::is_playable(const int ox, const int oy) const
{
	int x = ox, y = oy;
	const Tile me = (playerTurn == Turn::BLACK) ? Tile::BLACK : Tile::WHITE ;
	const Tile him = (playerTurn == Turn::BLACK) ? Tile::WHITE : Tile::BLACK;
	Tile current;

/* checking if game is in progress */
	if(playerTurn != Turn::BLACK && playerTurn != Turn::WHITE)
		return false;

/* checking for basic requirements for a turn to be valid */
	if(!is_in_bounds(x,y) || board[map(x,y)] != Tile::EMPTY)
		return false;
/* checking in each direction until one is playable */
	for(int i = 0; i < VECTORS_COUNT; i++ )
	{
		x = ox;
		y = oy;
		int steps = 0;
		do
		{
			steps++;
			x += direction[i].x;
			y += direction[i].y;
			current = get_board_value(x,y);
		} while (current == him);

		if(current == me && steps > 1)
			return true;

	}
/* no playable direction has been found */
	return false;
}

/*!
 * Examines each tile using is_playable() and returns true if at least one is
 * playable. If there is none, returns false.
 * \returns True if there is a move available, false if there is not.
 * \author Radek Vit
 */
bool icp::Board::move_available() const
{
	for(int y = 0;y < size; y++){
		for(int x=0;x < size; x++)
		{
			if(is_playable(x,y))
			{
				return true;
			}
		}
	}
	return false;
}

/*!
 * Examines each tile using is_playable(). If is_playable() returns true, adds
 * this tile's coordinates to vector v. After examining all tiles, vector v is
 * returned.
 * \returns Vector of all possible moves.
 * \author Radek Vit
 */
std::vector<icp::Coordinates> icp::Board::moves() const
{
	std::vector<Coordinates> v;
	for(int x = 0; x<size; x++)
	{
		for(int y=0; y<size; y++)
		{
			if(is_playable(x,y))
			{
				v.push_back({x,y});
			}
		}
	}
	return v;
}

/*!
 * An easy to parse way of saving Board's state. Board's size, history size and
 * history position is written divided by newlines, followed by all played tiles
 * from history.
 * \param[in] f Ofstream to which save data is written.
 * \author Radek Vit
 */
void icp::Board::save(std::ofstream &f)
{
	f << "\n" << size << "\n";
	f << history.size() << "\n"; //total history size
	f << positionInHistory << "\n";
	for(historyEntry e:history)
	{
		f << e.playedX << " " << e.playedY << "\n";
	}
}

/*!
 * Loads all of Board's saved state from ifstream f. Loading is safe against
 * savefile manipulation if this is the last data to be loaded from the
 * ifstream. Uses play() and redo() to safely reconstruct history. If no data is
 * left in ifstream upon running this method, history is cleared but the rest of
 * board stays the same.
 * \param[in] f Ifstream containing savefile data.
 * \author Radek Vit
 */
void icp::Board::load(std::ifstream &f) try
{
	int n_size=-1;
	unsigned int historySize = 0, historyPoint = 0;
	int x,y;

	f >> n_size; //resize board and reset
	if(n_size < MIN_SIZE || n_size > MAX_SIZE || n_size % 2 != 0)
		throw std::runtime_error("bad board size");
	size = n_size;
	board.resize(size*size);

	board.assign(size*size,Tile::EMPTY);
	board[map(size/2,size/2)] = Tile::WHITE;
	board[map(size/2 - 1,size/2 - 1)] = Tile::WHITE;
	board[map(size/2 - 1,size/2)] = Tile::BLACK;
	board[map(size/2,size/2 - 1)] = Tile::BLACK;

	history.clear();
	positionInHistory = 0;
	playerTurn = Turn::BLACK;

	f >> historySize >> historyPoint; //read length of history and position
	if(historySize < historyPoint)
		throw std::runtime_error("history position is larger than size");
	// play each
	for(unsigned int i = 0; i < historySize; i++)
	{
		f >> x;
		f >> y;
		play(x,y);
	}
	// roll back to saved state
	while(positionInHistory > historyPoint) //this is safe against bad savefiles
	{
		undo();
	}
}
catch(std::exception &e)
{
	std::string err = "Invalid save file: ";
	err += e.what();
	throw std::runtime_error(err);
}
/*** End of file board.cpp ***/
