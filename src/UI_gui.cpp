/*!
 * \file UI_gui.cpp
 *
 * \brief Contains GUI
 * \author Tomas Zencak
 */
#include <UI_gui.qt.h>
#include <QPushButton>
#include <QLabel>
#include <QSlider>
#include <QComboBox>
#include <QFileDialog>
#include <QGridLayout>
#include <QMessageBox>
#include <QCloseEvent>
#include <chrono>
#include <thread>

namespace icp
{
/*!
 * \brief size of one tile in pixels
 */
	const int square_size=40;

/*!
 * \author Tomas Zencak
 */
	GuiMenu::GuiMenu(UiGui *ui):
		QWidget(nullptr),
		ui(ui),
		layout(new QGridLayout(this))
	{
		resize(300, 300);
		setAttribute(Qt::WA_DeleteOnClose);
		
		new_game_button=new QPushButton("New game", this);
		load_game_button=new QPushButton("Load game", this);
		quit_game_button=new QPushButton("Quit game", this);
		errors = new QMessageBox(this);
		errors->setIcon(QMessageBox::Warning);
		layout->addWidget(new_game_button, 0, 0);
		layout->addWidget(load_game_button, 1, 0);
		layout->addWidget(quit_game_button, 2, 0);
		connect(quit_game_button, &QPushButton::clicked, [&](bool){this->ui->quit();});
		connect(load_game_button, &QPushButton::clicked, this, &GuiMenu::load_game);
		connect(new_game_button, &QPushButton::clicked, [&](bool){new GuiSizePicker(this->ui);});
		show();
	}

	GuiMenu::~GuiMenu()
	{
		delete layout;
	}

/*!
 * \author Tomas Zencak
 */
	void GuiMenu::display_error(const QString &message)
	{
		this->errors->setText(message);
		this->errors->open();
	}

/*!
 * Displays a file dialog and loads from the given file
 * \author Tomas Zencak
 */
	void GuiMenu::load_game(bool)
	{
		QString str=QFileDialog::getOpenFileName();
		std::ifstream file(str.toStdString());
		if(file.fail())
		{
			display_error("Could not open file");
		}
		else
		{
			ui->load_game(file);
		}
	}

/*!
 * \author Tomas Zencak
 */
	void GuiMenu::closeEvent(QCloseEvent *evt)
	{
		ui->quit();
		evt->accept();
	}

/*!
 * \author Tomas Zencak
 */
	void GuiGame::init_layout()
	{
		int size=game.get_board().get_size();
		setFixedSize(250+size*square_size, 100+size*square_size);
		save_button=new QPushButton("Save game", this);
		change_players_button=new QPushButton("Change players", this);
		undo_button=new QPushButton("Undo", this);
		redo_button=new QPushButton("Redo", this);
		layout->addWidget(undo_button, 7, 0);
		layout->addWidget(redo_button, 7, 1);
		layout->addWidget(change_players_button, 7, 3);
		layout->addWidget(save_button, 7, 2);
		QLabel *label=new QLabel(this);
		label=new QLabel(this);
		label->setText("Black:");
		layout->addWidget(label, 5, 0,1,1, Qt::AlignRight);
		label=new QLabel(this);
		label->setText("White:");
		layout->addWidget(label, 6, 0,1,1, Qt::AlignRight);
		black_score=new QLabel(this);
		black_score->setNum(game.get_board().count_black());
		layout->addWidget(black_score, 5, 1);
		white_score=new QLabel(this);
		white_score->setNum(game.get_board().count_white());
		layout->addWidget(white_score, 6, 1);
		label=new QLabel(this);
		label->setText("CurrentPlayer");
		layout->addWidget(label, 5, 2);
		label=new QLabel(this);
		label->setText("Remaining stones:");
		layout->addWidget(label, 6, 2);
		remaining_stones=new QLabel(this);
		remaining_stones->setNum(size*size-4);
		layout->addWidget(remaining_stones, 6, 3);
		current_player=new QLabel(this);
		current_player->setText("Black");
		layout->addWidget(current_player, 5, 3);
		field_container=new QWidget(this);
		field_container->setFixedSize(size*square_size, size*square_size);
		layout->addWidget(field_container, 0, 0,1,4, Qt::AlignCenter);
		for(int y=0; y<size; y++)
		{
			for(int x=0; x<size; x++)
			{
				label=new	QLabel(field_container);
				label->setGeometry(x*square_size, y*square_size, square_size, square_size);
				fields.push_back(label);
			}
		}
		connect(save_button, &QPushButton::clicked, this, &GuiGame::save_game);
		connect(change_players_button, &QPushButton::clicked,
		[this](bool){this->player_picker = new GuiPlayerPicker(this);});
		connect(undo_button, &QPushButton::clicked, this, [&](bool){game.undo();});
		connect(redo_button, &QPushButton::clicked, this, [&](bool){game.redo();});

		display_fields();
	}

/*!
 * \author Tomas Zencak
 */
	void GuiGame::closeEvent(QCloseEvent *e)
	{
		delete this->player_picker;
		this->ui->end_game(this);
		QWidget::closeEvent(e);
	}

/*!
 * \author Tomas Zencak
 */
	void GuiGame::display_fields()
	{
		const Board &board=game.get_board();
		int size=board.get_size();
		for(int y=0; y<size; y++)
		{
			for(int x=0; x<size; x++)
			{
				QLabel *field=fields[x+y*size];
				switch(board.get_board_value(x, y))
				{
					case Tile::BLACK:
						field->setPixmap(ui->black_pic.scaled(field->width(), field->height(), Qt::KeepAspectRatio));
						break;
					case Tile::WHITE:
						field->setPixmap(ui->white_pic.scaled(field->width(), field->height(), Qt::KeepAspectRatio));
						break;
					case Tile::EMPTY:
						field->setPixmap((board.is_playable(x, y) ? ui->available_pic : ui->empty_pic).scaled(field->width(), field->height(), Qt::KeepAspectRatio));
						break;
				}
			}
		}
	}

/*!
 * \author Tomas Zencak
 */
	void GuiGame::save_game(bool)
	{
		QString savefile=QFileDialog::getSaveFileName(this, "Save game");
		std::ofstream file(savefile.toStdString());
		if(file.fail())
		{
			ui->get_menu_ptr()->display_error("Failed to open the file");
		}
		else
		{
			game.save(file);
		}
	}

/*!
 * \author Tomas Zencak
 */
	void GuiGame::change_players(bool white_is_human, int ai_index)
	{
		if(white_is_human)
		{
			game.change_white(0);
			game.change_black(ai_index);
		}
		else
		{
			game.change_black(0);
			game.change_white(ai_index);
		}
	}

/*!
 * \author Tomas Zencak
 */
	void GuiGame::generate_buttons()
	{
		for(auto button:play_buttons)
		{
			delete button;
		}
		play_buttons.clear();
		std::vector<Coordinates> possible_moves(game.get_board().moves());
		int size=game.get_board().get_size();
		for(auto move:possible_moves)
		{
			QPushButton *button=new QPushButton(field_container);
			button->setGeometry(fields[move.x+move.y*size]->geometry());
			button->setFlat(true);	
			//button->setAttribute(Qt::WA_TranslucentBackground, true);
			connect(button, &QPushButton::clicked, [move, this](bool){game.play(move.x, move.y);});
			button->show();
			play_buttons.push_back(button);
		}
	}


/*!
 * Calls game.play() and updates the GUI if necessary
 * \author Tomas Zencak
 */
	void GuiGame::tick()
	{
		Status status=game.play();
		const Board &board=game.get_board();
		int new_black_score=board.count_black();
		int new_white_score=board.count_white();
		//use this to detect change in board
		if(new_black_score+new_white_score!=prev_stone_count)
		{
			black_score->setNum(new_black_score);
			white_score->setNum(new_white_score);
			prev_stone_count=new_black_score+new_white_score;
			switch(board.player())
			{
				case Turn::BLACK:
					current_player->setText("Black");
					break;
				case Turn::WHITE:
					current_player->setText("White");
					break;
				case Turn::BLACKWIN:
					current_player->setText("Black wins");
					break;
				case Turn::WHITEWIN:
					current_player->setText("White wins");
					break;
				case Turn::TIE:
					current_player->setText("It's a tie");
					break;
			}
			remaining_stones->setNum(board.get_size()*board.get_size()-new_black_score-new_white_score);
			display_fields();
			if(status==Status::READY)
			{
				generate_buttons();
				undo_button->setEnabled(board.undo_available());
				redo_button->setEnabled(board.redo_available());
				save_button->setEnabled(true);
				change_players_button->setEnabled(true);
			}
			else
			{
				//if it's not player's turn, unconditionally disable all buttons
				undo_button->setEnabled(false);
				redo_button->setEnabled(false);
				save_button->setEnabled(false);
				change_players_button->setEnabled(false);
			}
		}
	}

/*!
 * \author Tomas Zencak
 */
	GuiGame::GuiGame(UiGui *ui, int size):
		QWidget(nullptr),
		game(size),
		ui(ui),
		layout(new QGridLayout(this))
	{
		setAttribute(Qt::WA_DeleteOnClose);
		init_layout();
		show();
		player_picker = new GuiPlayerPicker(this);
	}

/*!
 * \author Tomas Zencak
 */
	GuiGame::GuiGame(UiGui *ui, std::ifstream &savefile):
		QWidget(nullptr),
		game(),
		ui(ui),
		layout(new QGridLayout(this))
	{
		setAttribute(Qt::WA_DeleteOnClose);
		game.load(savefile);
		init_layout();
		show();
	}

/*!
 * \author Tomas Zencak
 */
	GuiSizePicker::GuiSizePicker(UiGui *ui):
		ui(ui),
		num(8),
		layout(new QGridLayout(this))
	{
		setAttribute(Qt::WA_DeleteOnClose);
		ui->get_menu_ptr()->setEnabled(false);
		resize(110, 110);
		hint = new QLabel(this);
		hint->setText("Choose board size:");
		layout->addWidget(hint, 0, 0);
		slider=new QSlider(Qt::Orientation::Horizontal, this);
		slider->setMinimum(3);
		slider->setMaximum(6);
		slider->setSingleStep(1);
		slider->setValue(4);
		layout->addWidget(slider, 1, 0);
		numdisp=new QLabel(this);
		numdisp->setNum(8);
		layout->addWidget(numdisp, 2, 0);
		connect(slider, &QSlider::valueChanged, this, &GuiSizePicker::setNum);
		accept_button=new QPushButton("OK", this);
		layout->addWidget(accept_button, 3, 0);
		connect(accept_button, &QPushButton::clicked, 
				[&](bool)
				{
					this->ui->run_game(num);
					this->ui->get_menu_ptr()->setEnabled(true);
					this->close();
				}
			);
		show();
	}

/*!
 * \author Tomas Zencak
 */
	GuiSizePicker::~GuiSizePicker()
	{
		ui->get_menu_ptr()->setEnabled(true);
	}

/*!
 * \author Tomas Zencak
 */
	void GuiSizePicker::setNum(int val)
	{
		num=val*2;
		numdisp->setNum(val*2);
	}

/*!
 * \author Tomas Zencak
 */
	GuiPlayerPicker::GuiPlayerPicker(GuiGame *game):
		game(game),
		layout(new QGridLayout(this))
	{
		setAttribute(Qt::WA_DeleteOnClose);
		game->setEnabled(false);
		QLabel *label=new QLabel(this);
		label->setText("Human player color");
		layout->addWidget(label);
		human_picker=new QComboBox(this);
		human_picker->addItem("White", true);
		human_picker->addItem("Black", false);
		layout->addWidget(human_picker, 0, 1);
		label=new QLabel(this);
		label->setText("Other player");
		layout->addWidget(label, 0, 2);
		player_type_picker=new QComboBox(this);
		for(auto item:Factory_ai::options)
		{
			player_type_picker->addItem(QString::fromStdString(item.name), item.index);
		}
		layout->addWidget(player_type_picker, 0, 3);
		accept_button=new QPushButton("OK", this);
		layout->addWidget(accept_button, 0, 4);
		connect(accept_button, &QPushButton::clicked, this, &GuiPlayerPicker::Pick);
		show();
	}

/*!
 * \author Tomas Zencak
 */
	void GuiPlayerPicker::Pick(bool)
	{
		game->change_players(human_picker->currentData().toBool(), player_type_picker->currentData().toInt());
		game->setEnabled(true);
		this->close();
	}

/*!
 * \author Tomas Zencak
 */
	GuiPlayerPicker::~GuiPlayerPicker()
	{
		game->player_picker = nullptr;
		game->setEnabled(true);
	}

/*!
 * \author Tomas Zencak
 */
	UiGui::UiGui(int argc, char **argv):
		app(argc, argv),
		running(true),
		menu_window(this)
	{
		black_pic.load("img/black.png");
		white_pic.load("img/white.png");
		available_pic.load("img/available.png");
		empty_pic.load("img/empty.png");	
		menu_window.show();
	}

/*!
 * \author Tomas Zencak
 */
	UiGui::~UiGui()
	{
	}

/*!
 * \author Tomas Zencak
 */
	void UiGui::run_game(int size)
	{
		games.push_back(new GuiGame(this, size));
	}

/*!
 * \author Tomas Zencak
 */
	void UiGui::load_game(std::ifstream &savefile)
	{
		games.push_back(new GuiGame(this, savefile));
	}

/*!
 * Erases the game from the GUI's list of games
 * \author Tomas Zencak
 */
	void UiGui::end_game(GuiGame *game)
	{
		for(auto iter=games.begin(); iter!=games.end(); iter++)
		{
			if(*iter==game)
			{
				games.erase(iter);
				return;
			}
		}
	}

/*!
 * Standard game loop, calls Qt's processEvents and ticks
 * \author Tomas Zencak
 */
	void UiGui::run()
	{
		using clock=std::chrono::high_resolution_clock;
		while(running)
		{
			try
			{
				auto frame_start_time=clock::now();
				app.processEvents();
				tick();
				auto next_frame_time=frame_start_time+std::chrono::milliseconds(40);
				if(next_frame_time>clock::now())
				{
					std::this_thread::sleep_until(next_frame_time);
				}
			}
			catch(std::exception &e)
			{
				menu_window.display_error(e.what());
			}
		}
	}

/*!
 * \author Tomas Zencak
 */
	void UiGui::tick()
	{
		for(auto game:games)
		{
			game->tick();
		}
	}
}
