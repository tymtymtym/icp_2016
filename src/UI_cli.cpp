/*!
 * \file UI_cli.cpp
 *
 * \brief Implements methods of class UiCli.
 * \author Radek Vit
 * \author Tomas Zencak
 */
#include <UI_cli.h>
#include <sstream>
#include <board.h>
#include <iostream>
#include <ai.h>
#include <cstdlib>
#include <thread>
#include <chrono>
#include <stdexcept>

namespace icp
{
/*!
 * Displays menu. Lets user start a new game, load a game or exit. When a game
 * is in progress, lets user continue or change the game's players.
 * \author Tomas Zencak
 * \author Radek Vit
 */
	void UiCli::run()
	{
		std::string messg="";
		auto Ai_options = Factory_ai::options;
		Ai_options.erase(Ai_options.begin());
		while(true)
		{
			clear_screen();
			std::cout << "Othello\n";
			std::cout << "(n)ew game, (l)oad game, (q)uit game";
			if(game)
				std::cout << "\n(c)ontinue game, " <<
				"change (p)layers";
			std::cout << std::endl;
			if(messg != "")
			{
				std::cout << "Error: " << messg << std::endl;
			}
			std::cout<<"Input a character corresponding to an action: ";
			std::string input;
			std::cin.clear();
			std::cin>>input;
			while(input.length()!=1)
			{
				std::cout<<"Input only one character\n";
				std::cin>>input;
			}
			try
			{
				messg="";
				switch(input[0])
				{
					case 'n':
					{
						unsigned ai_id;
						int size;
						std::cout << "Choose board size:\n";
						std::cin >> size;
						std::cout <<
							 "Play against (h)uman, or (c)omputer?\n";
						std::cin >> input;
						if(input.length() != 1 ||
						 (input[0] != 'h' && input[0] != 'c'))
						{
							messg = "Bad input.";
							break;
						}
						if(input[0] == 'h')
						{
							run_game(0,0,size);
							event_loop();
							break;
						}
						std::cout <<
							"Computer plays as (X), or (O)?\n";
						std::cin >> input;
						if(input.length() != 1 ||
							 (std::toupper(input[0]) != 'X' &&
							  std::toupper(input[0]) != 'O'))
						{
							messg = "Bad input.";
							break;
						}
						std::cout << "Choose Ai:\n";
						for(auto option: Ai_options)
							std::cout << option.to_string();
						std::cin >> ai_id;
						while(ai_id == 0)
						{
							std::cout << "Input a number bigger than 0.\n";
							std::cin >> ai_id;
						}
						if(std::toupper(input[0]) == 'X')
							run_game(ai_id,0,size);
						else
							run_game(0,ai_id,size);
						event_loop();
					}
					break;	
					case 'p':
					{
						unsigned ai_id;
						if(!game)
						{
							messg = "No game in progress.";
							break;
						}
						std::cout <<
						 "Play against (h)uman, or (c)omputer?:\n";
						std::cin >> input;
						if(input.length() != 1 ||
						 (input[0] != 'h' && input[0] != 'c'))
						{
							messg = "Bad input.";
							break;
						}
						if(input[0] == 'h')
						{
							game->change_black(0);
							game->change_white(0);
							break;
						}
						std::cout << "Computer plays as (X), or (O)?\n";
							std::cin >> input;
						if(input.length() != 1 ||
							 (std::toupper(input[0]) != 'X' &&
							  std::toupper(input[0]) != 'O'))
						{
							messg = "Bad input.";
							break;
						}
						std::cout << "Choose Ai:\n";
						for(auto option: Ai_options)
							std::cout << option.to_string();
						std::cin >> ai_id;
						while(ai_id == 0)
						{
							std::cout << "Input a number bigger than 0.\n";
							std::cin >> ai_id;
						}
						if(std::toupper(input[0]) == 'X')
						{
							game->change_black(ai_id);
							game->change_white(0);
						}
						else
						{
							game->change_black(0);
							game->change_white(ai_id);
						}
					}
					break;	
					case 'q':
						return;
					case 'l':
					{
						std::cout << "Choose savefile:\n";
						std::cin >> std::ws;
						std::getline(std::cin, input);
						std::ifstream f(input);
						if(!f)
						{
							messg = "Could not open savefile.";
							break;
						}
						run_game(f);
						event_loop();
					}
					break;
					case 'c':
					{
						if(game)
							event_loop();
						else
							messg = "No game in progress.";
					}
					break;
				} //switch
			}
			catch(std::exception &e)
			{
				messg = e.what();
			}
		}
	}
/*!
 * Starts a new game from a savefile. If loading fails, nothing happens and the
 * application will return to menu.
 * \author Radek Vit
 */
	void UiCli::run_game(std::ifstream &f)
	{
		std::unique_ptr<Game> n_game{new Game()};

		n_game->load(f);

		game.swap(n_game);
	}
/*!
 * Starts a new game with given Ai or players. If anything fails, nothing
 * happens and the application will return to menu.
 * \author Radek Vit
 */
	void UiCli::run_game(unsigned black, unsigned white,int size)
	{
		std::unique_ptr<Game> n_game(new Game(size));
		n_game->change_black(black);
		n_game->change_white(white);

		game.swap(n_game);
	}
/*!
 * Main event loop of the game. While Ai is working it outputs that information
 * to the terminal and keeps redrawing the board. When human input is expected,
 * calls tick(). 
 * \author Radek Vit
 * \author Tomas Zencak
 */
	void UiCli::event_loop()
	{
		using clock=std::chrono::high_resolution_clock;
		using time_point=std::chrono::time_point<clock>;
		std::string dots = "";
		time_point next_dot_time;
		while(running)
		{
			Status status=game->play();
			draw_frame();
			if(status == Status::BUSY)
			{
				std::cout << "Ai working";
				if(dots == "....")
					dots = "";
				std::cout << dots << "\n";
				if(clock::now()>next_dot_time)
				{
					dots += ".";
					next_dot_time=clock::now()+std::chrono::milliseconds(300);
				}
				std::this_thread::sleep_for(std::chrono::milliseconds(150));
			}
			else
			{
				dots = "";
				tick();
			}
		}
		running = true;
	}
/*!
 * Builds all display elements to a string. Adds elements according to board's
 * state. Adds viable commands, the board and score.
 * \author Tomas Zencak
 * \author Radek Vit
 */
	void UiCli::build_display()
	{
		const Board &board = game->get_board();
		int black = board.count_black();
		int white = board.count_white();
		int left = board.get_size()*board.get_size() - black - white;
		std::stringstream builder;
		builder<<"Othello\n";
		builder<<"(p)lay, (q)uit to menu";
		builder<<", (s)ave game";
		if(board.undo_available())
		{
			builder<<", (u)ndo";
		}
		if(board.redo_available())
		{
			builder<<", (r)edo";
		}

		builder<<"\n";
		{
			auto prev_padding=builder.widen(' ');
			builder << "   ";
			for(int x=0; x < board.get_size(); x++)
			{
				char c = x + 'A';
				builder.width(2);
				builder<< c <<' ';
			}
			builder.widen(prev_padding);
			builder<<'\n';
		}
		for(int y=0; y<board.get_size(); y++)
		{
			builder.width(2);
			builder<<y+1<<' ';
			for(int x=0; x<board.get_size(); x++)
			{
				builder<<'[';
				switch(board.get_board_value(x, y))
				{
					case Tile::EMPTY:
						if(board.is_playable(x,y))
							builder<<':';
						else
							builder<<' ';
						break;
					case Tile::BLACK:
						builder<<'X';
						break;
					case Tile::WHITE:
						builder<<'O';
						break;
				}
				builder<<']';
			}
			builder<<'\n';
		}
		builder << "Score || ";
		builder << "X: " << black << " | ";
		builder << "O: " << white << " ||\n";
		builder << "Discs available: " << left << "\n";
		switch(board.player())
		{
			case Turn::BLACKWIN:
				builder<<"X wins";
				break;
			case Turn::WHITEWIN:
				builder<<"O wins";
				break;
			case Turn::TIE:
				builder<<"It's a tie";
				break;
			case Turn::BLACK:
				builder<<"X's turn";
				break;
			case Turn::WHITE:
				builder<<"O's turn";
				break;
		}
		builder<<'\n';
		display = builder.str();
	}
/*!
 * Builds display, clears the screen and draws display.
 * \author Tomas Zencak
 */	
	void UiCli::draw_frame()
	{
		build_display();
		clear_screen();
		std::cout<<display;
	}
/*!
 * Clears screen. Supports Windows and ANSI linux terminals.
 * \author Radek Vit
 */
	void UiCli::clear_screen()
	{
#ifdef _WIN32_
		std::system("cls");		
#else
		std::cout << "\033[2J\033[1;1H";
#endif
	}
/*!
 * Gets action input from user and performs the action.
 * \author Tomas Zencak
 * \author Radek Vit
 */	
	void UiCli::tick()
	{
		std::cout<<"Input a character corresponding to an action: ";
		std::string input;
		std::cin.clear();
		std::cin>>input;
		if(input.length()!=1)
		{
			return;
		}
		switch(input[0])
		{
			case 'q':
				running = false;
				return;
			case 's':
				{
					std::ofstream to_save;
					std::cout<<"Input name of file to save to: ";
					std::cin>>std::ws;
					std::getline(std::cin, input);
					to_save.open(input);
					if(!to_save.is_open())
					{
						throw std::runtime_error("File cannot be opened.");
					}
					game->save(to_save);
				}
				break;
			case 'u':
				game->undo();
				break;
			case 'r':
				game->redo();
				break;
			case 'p':
				{
					int row=0;
					std::string col="";
					std::cin.clear();
					std::cout<<"Enter column letter: ";
					std::cin>>col;
					std::cout<<"Enter row number: ";
					std::cin>>row;
					game->play(std::toupper(col[0])-'A',row-1);
				}
				break;
			default:
				break;
		}
	}

}
/*** End of file UI_cli.cpp ***/
