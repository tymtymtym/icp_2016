/*!
 * \file Game.cpp
 *
 * \brief Implements game core.
 * \author Radek Vit
 * \author Tomas Zencak
 */
#include <Game.h>
#include <factory_ai.h>
#include<stdexcept>
#include<thread>
#include<iostream>

namespace icp
{
/*!
 * Sets default values. No player is Ai, board is of size board_size. Ai thread
 * is IDLE.
 * \author Tomas Zencak
 */
	Game::Game(int board_size/*=8*/):
		black{nullptr},
		white{nullptr},
		black_id{0},
		white_id{0},
		board{board_size},
		ai_status{Ai_status::IDLE},
		ai_thread{&Game::ai_thread_func, this}
	{	}
/*!
 * Notifies Ai thread to terminate. After it joins, finishes destructor.
 * \author Tomas Zencak
 */
	Game::~Game()
	{
		std::unique_lock<std::mutex> lock{ai_mutex};
		ai_status.store(Ai_status::END, std::memory_order_release);
		ai_condvar.notify_all();
		lock.unlock();
		//wait for thread to join
		//will block, so may be undesirable
		ai_thread.join();
	}
/*!
 * Ai thread. When an Ai offer is requested, runs Ai::offer. Terminates when
 * ai_status is set to END.
 * \author Tomas Zencak
 */
	void Game::ai_thread_func()
	{
		class scoped_unlock
		{
			using lock_type=std::unique_lock<std::mutex>;
			lock_type &lock;
		public:
			scoped_unlock(lock_type &to_unlock):
				lock(to_unlock){lock.unlock();}
			~scoped_unlock()
				{lock.lock();}
		};
		std::unique_lock<std::mutex> lock(ai_mutex);
		while(true)
		{
			switch(ai_status.load(std::memory_order_acquire))
			{
				case Ai_status::REQUEST_PENDING:
					{
						ai_status.store(Ai_status::BUSY, std::memory_order_release);
						switch(board.player())
						{
							case Turn::BLACK:
								{
									scoped_unlock unlock(lock);
									move=black->offer(board);
								}
								break;
							case Turn::WHITE:
								{
									scoped_unlock unlock(lock);
									move=white->offer(board);
								}
								break;
							default:
								break;
						}
						Ai_status prev_status=ai_status.exchange(Ai_status::DONE, std::memory_order_acq_rel);
						//status may have been changed to END while computing
						if(prev_status==Ai_status::END)
						{
							return;
						}
					}
					break;
				case Ai_status::END:
					return;
				default:
					break;
			}
			ai_condvar.wait(lock);
		}
	}
/*!
 * Loads Ai and loads board. If loading fails, Ai stays the same.
 * \param[in] f Open ifstream to be loaded from.
 * \returns Status of Game after loading.
 * \author Radek Vit
 */
	Status Game::load(std::ifstream &f)
	{
		unsigned l_black,l_white;
		if(status()==Status::BUSY)
			throw std::runtime_error(
			 "Trying to load a game when Ai is working.");
		f >> l_black >> l_white;
		std::unique_ptr<Ai> black_t(factory.generate_ai(l_black));
		std::unique_ptr<Ai> white_t(factory.generate_ai(l_white));
		board.load(f);

		black_id = l_black;
		white_id = l_white;
		black.swap(black_t);
		white.swap(white_t);
		return status();
	}
/*!
 * Saves black and white ids and saves board.
 * \author Radek Vit
 */
	void Game::save(std::ofstream &f)
	{
		f << black_id << " " << white_id << "\n";
		board.save(f);
	}

/*!
 * If Ai is to play and is IDLE, notifies Ai thread and returns BUSY.
 * If Ai is not to play and is IDLE, returns READY.
 * If Ai is working, returns BUSY.
 * If Ai is ready, makes the offered turn. If the next player is Ai, notifies
 * Ai thread and returns BUSY. If the next player is not Ai, returns READY.
 * \author Tomas Zencak
 */
	Status Game::play()
	{
		switch(ai_status.load(std::memory_order_acquire))
		{
		case Ai_status::DONE:
			board.play(move.x, move.y);
			ai_status.store(Ai_status::IDLE, std::memory_order_release);
			if(current_player_is_ai())
			{
				ai_offer();
				return Status::BUSY;
			}
			else
			{
				return Status::READY;
			}
		case Ai_status::IDLE:
			if(current_player_is_ai())
			{
				ai_offer();
				return Status::BUSY;
			}
			else
			{
				return Status::READY;
			}
		default:
			return Status::BUSY;
		}
	}
/*!
 * If Ai is IDLE and player should play, plays as player. If next player is
 * player, returns READY, otherwise notifies Ai thread and returns BUSY.
 * If Ai is IDLE and is to play, notifies Ai thread and returns BUSY.
 * If Ai is working, returns BUSY.
 * If Ai is DONE, plays offered turn. If the next player is Ai, notifies Ai
 * thread and returns BUSY, otherwise returns READY.
 * \author Tomas Zencak
 */
	Status Game::play(const int x,const int y)
	{
		switch(ai_status.load(std::memory_order_acquire))
		{
		case Ai_status::DONE:
			board.play(move.x, move.y);
			ai_status.store(Ai_status::IDLE, std::memory_order_release);
			if(current_player_is_ai())
			{
				ai_offer();
				return Status::BUSY;
			}
			else
			{
				return Status::READY;
			}
		case Ai_status::IDLE:
			board.play(x, y);
			if(current_player_is_ai())
			{
				ai_offer();
				return Status::BUSY;
			}
			else
			{
				return Status::READY;
			}
		default:
			return Status::BUSY;
		}
	}
/*!
 * If Game's Status is BUSY, this is an empty operation.
 * Otherwise undos board to the last turn when a player is to play. Ai turns
 * are skipped back to allow actual undo for the player.
 * \author Radek Vit
 */
	void Game::undo()
	{
		switch(status())
		{
		case Status::READY:
			do
			{
				board.undo();
			}
			while(board.undo_available() && current_player_is_ai());
		default:
			{}
		}
	}
/*!
 * If Game's status is BUSY, this is an empty operation.
 * Otherwise redos board to a turn where a player is to play. Ai turns are
 * skipped to not destroy history and to allow actual redo for the player.
 * \author Radek Vit
 */
	void Game::redo()
	{
		switch(status())
		{
		case Status::READY:
			do
			{
				 board.redo();
			}
			while(board.redo_available() && current_player_is_ai());
		default:
			{}
		}
	}
/*!
 * Notifies Ai thread that an offer is requested.
 * \author Tomas Zencak
 */
	void Game::ai_offer()
	{
		/* one thread is offering move, other returns */
		std::unique_lock<std::mutex> lock(ai_mutex);
		ai_status.store(Ai_status::REQUEST_PENDING, std::memory_order_release);
		ai_condvar.notify_all();
	}

}
/*** End of file Game.cpp ***/
