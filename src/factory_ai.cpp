/*!
 * \file factory_ai.cpp
 *
 * \brief Contains definitions of class Factory_ai methods and attributes
 * \author Radek Vit
 */
#include<factory_ai.h>
#include<stdexcept>
#include<vector>

/*!
 * \brief Offers all factory production options as pairs of
 * \brief explicit index and text description.
 */
const std::vector<icp::Ai_selection> icp::Factory_ai::options = 
{
	{0,"Player"},
	{1,"Alphabeta: easy"},
	{2,"Alphabeta: hard"},
	{3,"Aplhabeta: insane"},
	{4,"Random"},
	/* new oprions go here */
};

/*!
 * Produces an Ai subclass. Explicit indexes allow removing options in a way
 * that keeps other options' indexes untouched.
 * \author Radek Vit
 */
icp::Ai *icp::Factory_ai::new_ai(unsigned ai_number)
{
	switch(ai_number)
	{
		case 0: //reserved for player
			return nullptr;
		case 1:
			return new Ai_alphabeta(2);
		case 2:
			return new Ai_alphabeta(4);
		case 3:
			return new Ai_alphabeta(6);
		case 4:
			return new Ai_random();
		/* new options go here */
		default:
			throw std::runtime_error("Unknown AI number");
	}
}

/*** End of file factory_ai.cpp ***/
