/*!
 * \file main_gui.cpp
 *
 * \brief Contains main for hra2016.
 */
#include <UI_gui.qt.h>
/*!
 * \brief Runs UiGui::menu() and returns 0 when it's done.
 */
int main(int argc, char **argv)
{
	icp::UiGui ui(argc, argv);
	ui.run();
	return 0;
}
/*** End of file main_gui.cpp ***/
