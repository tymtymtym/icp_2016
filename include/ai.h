/*!
 * \file ai.h
 *
 * \brief Defines Ai classes and Board_ai class
 * \author Radek Vit
 */

#ifndef AIHEADEROMG
#define AIHEADEROMG
#include"board.h"
#include<stdexcept>
#include<ctime>
#include<random>
#include<vector>

namespace icp
{
/*!
 * \brief Represents who's turn it is. Used by Turn interface.
 */

	enum aiTurn {
	PLAYER1,
	PLAYER2,
	PLAYER1WIN,
	PLAYER2WIN,
	TIE,
	};

/*!
 * \brief Board with turn interface and heuristic.
 * \author Radek Vit
 */
	class Board_ai: public Board
	{
	protected:
		bool amIWhite;
		const int TIE_HEURISTIC = 0;
		const int WEIGHT = 4; //weight of bad and good spots on board

		std::vector<Coordinates> bad;
		std::vector<Coordinates> good;
		std::vector<Coordinates> best;

	public:
		Board_ai() = delete;
/*!
 * \brief Copy constructor from Board. Initialises vectors for heuristic().
 */
		Board_ai(const Board &b):
			Board(b),
			bad{
				{0,1}, {1,0}, {0,1}, {1,0},
				{1}, {1},
				{size-2,size-1}, {size-1,size-2},
				{size-2,size-1}, {size-1,size-2},
				{size-2}, {size-2},
				{0,size-2},{1,size-1}, {0,size-2},{1,size-1},
				{1,size-2}, {1,size-2},
				{size-2,0}, {size-1,1}, {size-2,0}, {size-1,1},
				{size-2,1}, {size-2,1}
			},
			good{
				{0,2}, {0,size-3}, {2,size-1}, {size-3,size-1},
				{size-1,size-3}, {size-1,3}, {size-3,0}, {2,0}
			},
			best{
				{0}, {size-1}, {0,size-1}, {size-1,0}, //corners
			}
		{
			Turn turn = b.player();
			amIWhite = (turn == Turn::WHITE) ? true : false;
			history.clear();
			positionInHistory = 0;

			for(int i = 1;i<size-1;i++) //adding bad positions
			{
				bad.push_back({1,i});
				bad.push_back({i,1});
				bad.push_back({size-2,i});
				bad.push_back({i,size-2});
			}
		}
		~Board_ai() {}
/*!
 * \brief Rates board situation from Board_ai's player's perspective.
 */
		int heuristic();

/*!
 * \brief Interface for player().
 */
		aiTurn player_ai()
		{
			if(amIWhite)
				switch(player())
				{
				case Turn::WHITE:
					return PLAYER1;
				case Turn::BLACK:
					return PLAYER2;
				case Turn::WHITEWIN:
					return PLAYER1WIN;
				case Turn::BLACKWIN:
					return PLAYER2WIN;
				case Turn::TIE:
					return TIE;
				}
			else
				switch(player())
				{
				case Turn::WHITE:
					return PLAYER2;
				case Turn::BLACK:
					return PLAYER1;
				case Turn::WHITEWIN:
					return PLAYER2WIN;
				case Turn::BLACKWIN:
					return PLAYER1WIN;
				case Turn::TIE:
					return TIE;
				}
			return TIE;
		}
	};

/*!
 * \brief Pure virtual class for Ai classes
 * \author Radek Vit
 */
	class Ai
	{
	public:
		Ai() {};
		virtual ~Ai() {}
/*!
 * \brief Offers a move for Board b.
 */
		virtual Coordinates offer(const Board &b) = 0;
	};

/*!
 * \brief Ai class offering moves using alphabeta algorhitm.
 * \author Radek Vit
 */
	class Ai_alphabeta: public Ai
	{
	protected:
		const int MAX_DEPTH;
		const int INCREMENT_SMALL = 1;
		const int INCREMENT_BIG = 2;
	public:
/*!
 * \brief Constructor setting alphabeta's maximum depth.
 */
		Ai_alphabeta(int depth=4): MAX_DEPTH(depth) {}
/*!
 * \brief Offers a move using alphabeta.
 */
		virtual Coordinates offer(const Board &b)
		{
			if(b.player() != Turn::WHITE && b.player() != Turn::BLACK)
				throw std::runtime_error(
					"Ai cannot offer a move for a finished game");
			return first(b,MAX_DEPTH);
		}

	protected:
/*!
 * \brief Part of alphabeta; alpha returning Coordinates.
 */
		Coordinates first(const Board &g,int max_depth);

/*!
 * \brief Alpha from alphabeta algorhitm.
 */
		int alpha(Board_ai &,int a,int b,int depth,int max_depth);

/*!
 * \brief Beta from alphabeta algorhitm.
 */
		int beta(Board_ai &,int a,int b,int depth,int max_depth);
	};

/*!
 * \brief Ai class offering moves using RNG.
 */
	class Ai_random: public Ai
	{
		std::mt19937 rng;
	public:
		Ai_random() { rng.seed(time(nullptr)); }
/*!
 * \brief Offers a move using RNG.
 */
		virtual Coordinates offer(const Board &b);
	};

}

#endif
/*** End of file ai.h ***/
