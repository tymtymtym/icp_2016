/*!
 * \file UI.h
 *
 * \brief Contains abstract class UI
 * \author Tomas Zencak
 */
#ifndef OTHELLO_UI_H_
#define OTHELLO_UI_H_

#include <board.h>
#include <vector>
#include <fstream>
#include <memory>

namespace icp
{
/*!
 * \brief Abstract base for user interfaces
 */
	class UI
	{
	public:
		UI() {};
		virtual ~UI(){}
/*!
 * \brief Draws the menu.
 */
		virtual void run()=0;

	};

}

#endif //OTHELLO_UI_H_
/*** End of file UI.h ***/
