/*!
 * \file Game.h
 *
 * \brief Contains class Game and resources for its interface.
 * \author Radek Vit
 * \author Tomas Zencak
 */
#ifndef GAME_H_
#define GAME_H_

#include"board.h"
#include"ai.h"
#include"factory_ai.h"
#include <atomic>
#include <mutex>
#include <thread>
#include <condition_variable>
#include <memory>

namespace icp
{
/*!
 * \brief A part of the interface for class Game;
 * READY suggests Game is awaiting commands.
 * BUSY suggests Game is to be called to do what it needs to do.
 */
	enum class Status
	{
		READY,
		BUSY
	};
/*!
 * \brief A control interface class Board, ensuring integrity and managing Ai.
 * Manages Ais. If Ai is to turn, computes its turn in another thread and
 * notifies its caller by returning Status::BUSY. Otherwise notifies caller
 * about awaiting instructions from caller by returning Status:: READY. This
 * class is the only one to manipulate with its board, a constant reference is
 * the only acceptable way to communicate its contents out of this class.
 * \author Radek Vit
 * \author Tomas Zencak
 */
	class Game
	{
		std::unique_ptr<Ai> black;
		std::unique_ptr<Ai> white;
/*!
 * \brief Keeps id of black Ai. Zero is reserved for no Ai.
 */
		unsigned black_id = 0;
/*!
 * \brief Keeps id of white Ai. Zero is reserved for no Ai.
 */
		unsigned white_id = 0;

		Factory_ai factory;

		Board board;
/*!
 * \brief Represents state of Ai's work.
 * \author Tomas Zencak
 */
		enum class Ai_status
		{
			IDLE,
			REQUEST_PENDING,
			BUSY,
			DONE,
			END
		};

		std::atomic<Ai_status> ai_status;
		std::mutex ai_mutex;
		std::condition_variable ai_condvar;
		Coordinates move;
		std::thread ai_thread;
/*!
 * \brief Function running in another thread. Realises offers from Ai.
 */
		void ai_thread_func();

	public:
/*!
 * \brief Constructs game with given size.
 */
		Game(int board_size=8);
		~Game();
/*!
 * \brief Changes black Ai by generating it from factory.
 * \param[in] id ID of Ai to be assigned.
 * \returns Status of Game after changing the Ai.
 */
		Status change_black(unsigned id)
		{
			black = factory.generate_ai(id);
			black_id = id;
			return status();
		}

/*!
 * \brief Changes white Ai by generating it from factory.
 * \param[in] id ID of Ai to be assigned.
 * \returns Status of Game after changing the Ai.
 */
		Status change_white(unsigned id)
		{
			white = factory.generate_ai(id);
			white_id = id;
			return status();
		}
		
/*!
 * \brief Gives caller a constant reference to board.
 */
		const Board &get_board() {return board;}
/*!
 * \brief If READY, plays x,y. Else works with Ai.
 */
		Status play(const int x,const int y);
/*!
 * \brief If READY, does nothing and notifies caller. Else works with Ai.
 */
		Status play();
/*!
 * \brief Calls Board::undo() one or more times according to Ai presence.
 */
		void undo();
/*!
 * \brief Cass Board::redo() one or more times according to Ai presence.
 */
		void redo();
/*!
 * \brief Saves Ai ids and calls Board::save().
 */
		void save(std::ofstream &save_to);
/*!
 * \brief Loads Ai from file and calls Board::load().
 */
		Status load(std::ifstream &load_from);
protected:
/*!
 * \brief Returns Status value according to Ai state and current player.
 * \returns Current status of this instance.
 * \author Tomas Zencak
 */
		Status status()
		{
			switch(ai_status.load(std::memory_order_relaxed))
			{
			case Ai_status::IDLE:
				break;
			default:
				return Status::BUSY;
			}
			return current_player_is_ai() ? Status::BUSY : Status::READY;
		}
/*!
 * \brief Returns true when current player is represented by Ai.
 * \author Tomas Zencak
 */
		bool current_player_is_ai()
		{
			switch(board.player())
			{
			case Turn::BLACK:
				return black_id != 0;
			case Turn::WHITE:
				return white_id != 0;
			default: 
				return false;
			}
		}

/*!
 * \brief Makes Ai offer a move for the current board state.
 */
		void ai_offer();
	};
}

#endif //GAME_H_
/*** End of file Game.h ***/
