/*!
 * \file board.h
 * 
 * \brief Contains class Board definition and implements inline methods
 * \author Radek Vit
 */
#ifndef GAMEHEADERWHATUP
#define GAMEHEADERWHATUP

#include<vector>
#include<fstream>
#include<utility>
#include<stdexcept>

namespace icp
{
/*!
 * \brief Represents turn and end-state possibilities of class Board.
 * \author Radek Vit
 */
	enum class Turn {
		BLACK,
		WHITE,
		BLACKWIN,
		WHITEWIN,
		TIE,
	};

/*!
 * \brief Represents tile states of board in class Board.
 * \author Radek Vit
 */
	enum class Tile {
		BLACK,
		WHITE,
		EMPTY,
	};
	
/*!
 * \brief Structure containing a pair of integers representing tile coordinates.
 * \author Radek Vit
 */
	struct Coordinates
	{
		int x=0,y=0;
		Coordinates(): x(0), y(0) {}
		Coordinates(int z): x(z), y(z) {}
		Coordinates(int x,int y): x(x), y(y) {}
		bool operator==(const Coordinates &rhs) const
			{return x==rhs.x && y==rhs.y;}
	};
/*! number of possible directions */
	const int MAX_ENDPOINTS = 8;

/*!
 * \brief Structure containing tile played and endpoints (each split to X and Y).
 * \author Radek Vit
 */
	struct historyEntry
	{
		int playedX;
		int playedY;
		int endPointsX[MAX_ENDPOINTS];
		int endPointsY[MAX_ENDPOINTS];
	};


/*!
 * \brief Class representing an Othello board with history.
 * \author Radek Vit
 * Othello board representation. Turns not obeying the rules are impossible.
 * Is aware of who is on turn. Is capable or undo and redo.
 * Size represents size of each dimension, number of tiles is size*size.
 */
	class Board 
	{
	public:
/*!
 * \brief Minimum size of Board.
 */
		const static int MIN_SIZE = 6;
/*!
 * \brief Maximum size of Board.
 */
		const static int MAX_SIZE = 12;

	protected:
		Turn playerTurn = Turn::BLACK;
		int size;
		std::vector<Tile> board;
	
		std::vector<historyEntry> history;
		size_t positionInHistory=0;

/*!
 * \brief Maps x,y to index in board.
 */
		int map(int x,int y) const {return size*y + x;}

	public:
		Board() = delete;
/*!
 * \brief Constructor with explicit size of board.
 * Sets the initial state of board.
 * \param[in] boardsize Sets size of board.
 */
		Board(int boardsize): size(boardsize)
		{
			if(boardsize < MIN_SIZE || boardsize > MAX_SIZE ||
				boardsize % 2 != 0)
				throw std::runtime_error("Bad board dimensions.");
			board.assign(size*size,Tile::EMPTY);
			board[map(size/2,size/2)] = Tile::WHITE;
			board[map(size/2 - 1,size/2 - 1)] = Tile::WHITE;
			board[map(size/2 - 1,size/2)] = Tile::BLACK;
			board[map(size/2,size/2 - 1)] = Tile::BLACK;
		}

		~Board() {}
/*!
 * \brief Returns size of board.
 */
		int get_size() const {return size;}
/*!
 * \brief Returns pointer to data of board.
 */
		const Tile *get_board_data() {return &board[0];}

/*!
 * \brief Plays on tile with coordinates x,y.
 */
		Turn play(const int ox,const int oy);

/*!
 * \brief Returns whichever player is on turn or who won.
 */
		Turn player() const {return playerTurn;}

/*!
 * \brief Undos last made play while keeping history intact.
 */
		Turn undo();

/*!
 * \brief Returns true if undo() will change Board's state.
 * \author Radek Vit
 */
		bool undo_available() const {return (positionInHistory != 0); }

/*!
 * \brief Redos last undone turn using history.
 */
		Turn redo();

/*!
 * \brief Returns true if redo() will change Board's state.
 * \author Radek Vit
 */
		bool redo_available() const {return (positionInHistory != history.size()); }

/*!
 * \brief saves Board state to open ofstream f
 */
		void save(std::ofstream &f);
/*!
 * \brief loads Board state from open ifstream f
 */
		void load(std::ifstream &f);

/*!
 * \brief Returns 1 on positive integer, 0 on zero integer and -1 on negative integer.
 */
		static int unitise(int x) {return (x==0)?0:((x<0)?-1:1);}

/*!
 * \brief Returns a vector of all possible moves
 */
		std::vector<Coordinates> moves() const;

/*!
 * \brief Safe tile extraction from board. Returns Tile::Empty on invalid x,y
 * \author Radek Vit
 */
		Tile get_board_value(const int x,const int y) const
		{
			if(is_in_bounds(x,y))
				return board[map(x,y)];
			else
				return Tile::EMPTY;
		}

/*!
 * \brief Returns true if tile x,y is playable for player on turn.
 */
		bool is_playable(const int ox,const int oy) const;

/*!
 * \brief Counts black tiles on board.
 */
 		int count_black() const
		{
			int count = 0;
			for(Tile tile:board)
			{
				if(tile == Tile::BLACK)
					count++;
			}
			return count;
		}

/*!
 * \brief Counts white tiles on board.
 */
 		int count_white() const
		{
			int count = 0;
			for(Tile tile:board)
			{
				if(tile == Tile::WHITE)
					count++;
			}
			return count;
		}

	protected:

/*!
 * \brief Returns true only if current player has at least one move available.
 */
		bool move_available() const;

/*!
 * \brief Returns true if tile x,y is placed inside the board.
 */
		bool is_in_bounds(const int x,const int y) const
		{
			return (x>=0 && y>=0 && x<size && y<size);
		}

	}; //class Board
}

namespace std
{
	template <> struct hash<icp::Coordinates>
	{
	public:
		size_t operator()(const icp::Coordinates &coords) const
		{
			return coords.x^(coords.y<<4);
		}
	};
}

#endif
/*** End of file board.h ***/
