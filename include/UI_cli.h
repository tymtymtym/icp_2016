/*!
 * \file UI_cli.h
 *
 * \brief Contains UiCli class.
 * \author Radek Vit
 * \author Tomas Zencak
 */
#ifndef UI_CLI_H_
#define UI_CLI_H_

#include <UI.h>
#include <string>
#include <unordered_set>
#include <memory>
#include <Game.h>
#include <board.h>
#include <fstream>

namespace icp
{
/*!
 * \brief Ui class for terminal output.
 */	
	class UiCli:public UI
	{
		std::unique_ptr<Game> game{nullptr};
		bool running=true;
		std::string display;
		void clear_screen();
		void build_display();
	public:
		UiCli() {}
		~UiCli() {}
/*!
 * \brief Displays menu in text form, allows controlling running game.
 */
		virtual void run() override;
/*!
 * \brief Creates new game with given Ai and size
 */
		virtual void run_game(unsigned black, unsigned white, int size);
/*!
 * \brief Creates new game from savefile
 */
		virtual void run_game(std::ifstream &f);
/*!
 * \brief Event loop when game is run. Lets user play when he should.
 */
		virtual void event_loop();
/*!
 * \brief Clears terminal and draws board in its current state.
 */
		virtual void draw_frame();
/*!
 * \brief Lets user make a command when on turn.
 */
		virtual void tick();

	};
}

#endif //UI_CLI_H_
/*** End of file UI_cli.h ***/
