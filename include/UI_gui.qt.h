/*!
 * \file UI_gui.h
 *
 * \brief Contains GUI UI
 * \author Tomas Zencak
 */

#ifndef UI_GUI_H_
#define UI_GUI_H_

#include <QWidget>
#include <QApplication>
#include <QPixmap>
#include <UI.h>
#include <Game.h>
#include <vector>

class QPushButton;
class QLabel;
class QSlider;
class QComboBox;
class QGridLayout;
class QMessageBox;

namespace icp
{

	class UiGui;
	class GuiSizePicker;

	/*!
	 * \brief The main game menu
	 * \author Tomas Zencak
	 */
	class GuiMenu:public QWidget
	{
		Q_OBJECT
		QPushButton *new_game_button;
		QPushButton *load_game_button;
		QPushButton *quit_game_button;
		UiGui *ui;
		QGridLayout *layout;
		QMessageBox *errors;
	public:
		GuiMenu(UiGui *ui);
		~GuiMenu();
		/*!
		 * \brief Displays an error dialog
		 */
		void display_error(const QString &message);
		/*!
		 * \brief Loads a game
		 */
		void load_game(bool);
		/*!
		 * \brief Starts a new game
		 */
		void new_game(bool);
		/*!
		 * \brief Executes on closing
		 */
		virtual void closeEvent(QCloseEvent *evt) override;
	};

	class GuiPlayerPicker;

	/*!
	 * \brief The game window
	 */
	class GuiGame:public QWidget
	{
		Game game;
		QPushButton *save_button;
		QPushButton *change_players_button;
		QPushButton *undo_button;
		QPushButton *redo_button;
		QLabel *black_score;
		QLabel *white_score;
		QLabel *remaining_stones;
		QLabel *current_player;
		QWidget *field_container;
		std::vector<QLabel*> fields;
		std::vector<QPushButton*> play_buttons;
		UiGui *ui;
		int prev_stone_count=-1;
		QGridLayout *layout;
		/*!
		 * \brief Inits the UI layout of the game
		 */
		void init_layout();
		/*!
		 * \brief Saves the game
		 */
		void save_game(bool);
		/*!
		 * \brief Updates the field images
		 */
		void display_fields();
		/*!
		 * \brief Generates a new set of playing buttons
		 */
		void generate_buttons();
	public:
		GuiPlayerPicker *player_picker = nullptr;
		/*!
		 * \brief Creates a new game of a given size
		 */
		GuiGame(UiGui *ui, int size);
		/*!
		 * \brief Loads a game from the given ifstream
		 */
		GuiGame(UiGui *ui, std::ifstream &savefile);
		/*!
		 * \brief Changes players
		 */
		void change_players(bool white_is_human, int ai_index);
		/*!
		 * \brief Does per tick processing
		 */
		void tick();
		/*!
		 * \brief Pre-close cleanup
		 */
		virtual void closeEvent(QCloseEvent *e) override;
	};
	
	/*!
	 * \brief Window for picking a player
	 */
	class GuiPlayerPicker:public QWidget
	{
		QComboBox *human_picker;
		QComboBox *player_type_picker;
		QPushButton *accept_button;
		GuiGame *game;
		QGridLayout *layout;
	public:
		GuiPlayerPicker(GuiGame *game);
		~GuiPlayerPicker();
		/*!
		 * \brief Picks a player
		 */
		void Pick(bool);
	};

	/*!
	 * \brief Window for picking game size
	 */
	class GuiSizePicker:public QWidget
	{
		QSlider *slider;
		UiGui *ui;
		QPushButton *accept_button;
		QLabel *numdisp;
		QLabel *hint;
		int num;
		QGridLayout *layout;
	public:
		GuiSizePicker(UiGui *ui);
		~GuiSizePicker();
		/*!
		 * \brief Sets size
		 */
		void setNum(int num);
	};

	class UiGui:public UI
	{
		QApplication app;
		bool running;
		GuiMenu menu_window;
		std::vector<GuiGame*> games;
	public:
		QPixmap black_pic, white_pic, empty_pic, available_pic;
		UiGui(int argc, char **argv);
		virtual ~UiGui();
/*!
 * \brief Runs the UI
 */
		virtual void run() override;
/*!
 * \brief Creates a new game with given size.
 */
		void run_game(int size);
/*!
 * \brief Creates a new game from given savefile.
 */
		void load_game(std::ifstream &);

		/*!
		 * \brief ends the given game
		 */
		void end_game(GuiGame *game);
/*!
 * \brief Gets events from the OS and does other per tick stuff.
 */
		void tick();

		/*!
		 * \brief Signals that the game should quit
		 */
		void quit()
		{
			running=false;
		}

		/*!
		 * \brief Gets pointer to main menu window
		 */
		GuiMenu *get_menu_ptr()
			{return &menu_window;}

	};


}

#endif //UI_GUI_H_
