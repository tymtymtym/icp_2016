/*!
 * \file factory_ai.h
 *
 * \brief Contains class Factory_ai and structure Ai_selection
 * \author Radek Vit
 */
#ifndef AI_FACTORY_H_
#define AI_FACTORY_H_

#include"ai.h"
#include<stdexcept>
#include<vector>
#include<string>
#include<memory>

namespace icp
{
/*!
 * \brief Used to store explicit identifiers and desctriptions of factory items.
 * \author Radek Vit
 */
	struct Ai_selection
	{
		unsigned index=0;
		std::string name = "";
		Ai_selection(unsigned i,std::string n): index(i), name(n) {}
/*!
 * \brief Converts structure to text representation.
 * \author Radek Vit
 */
		std::string to_string()
		{
			std::string s;
			s += std::to_string(index);
			s += "\t";
			s += name;
			s += "\n";
			return s;
		}
	};

/*!
 * \brief Factory producing to subclasses of Ai.
 * \author Radek Vit
 */
	class Factory_ai
	{
	public:
		static const std::vector<Ai_selection> options;

		Factory_ai() {}
		~Factory_ai() {}
		
/*!
 * \brief Returns a new object according to ai_number.
 */
		Ai *new_ai(unsigned ai_number);
/*!
 * \brief Constructs a unique_ptr from pointer returned by new_ai().
 */
		std::unique_ptr<Ai> generate_ai(unsigned ai_number)
		{
			return std::unique_ptr<Ai>(new_ai(ai_number));
		}

	};
}

#endif

/*** End of file factory_ai.h ***/
