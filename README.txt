ICP 2016 project
FIT VUT
Authors:
Radek Vit
Tomas Zencak

This project implements the game of Othello (Reversi).
To compile this project, make sure that PATH and LD_LIBRARY_PATH are set correctly
to Qt5.5.1. If hra2016 does not start after compiled, set the appropriate
qt.conf file in the directory with the game. hra2016 won't run correctly in a
folder where img/ is missing.
hra2016 is a GUI application requiring Qt to run.
hra2016-cli is a text interface application.
Savefiles from both binaries are compatible.
Example game right before a win is examples/save1

